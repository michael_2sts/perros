package com.example.dogapi

import android.os.Bundle
import android.telecom.Call
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.dogapi.ui.theme.DogApiTheme
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

class MainActivity : ComponentActivity() {

    lateinit var retrofit : Retrofit
    var texto: String = "Pruebas"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)

        setContent {
            DogApiTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(texto)
                }
            }

        }

    }

    private fun obtenerDatos(retrofit : Retrofit) : String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(dogApi::class.java).getMessage().execute()
            val perros = call.body()
            if(call.isSuccessful){
                texto = perros?.getMessage().toString()
            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(1000)
        return texto;
    }

}




@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    DogApiTheme {
        Greeting("Android")
    }
}