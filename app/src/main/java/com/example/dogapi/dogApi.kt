package com.example.dogapi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface dogApi  {

    @Headers("Accept: application/json")

    @GET("status")
    fun getStatus(): Call<Perro>

    @GET("breeds/image/random")
    fun getMessage(): Call<Perro>
}