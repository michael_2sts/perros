package com.example.dogapi

import com.google.gson.annotations.SerializedName

data class PerroResponse(
    @SerializedName("results") var results: ArrayList<Perro>
)
